﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVVM.ViewModel;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace ScrollviewBehavior
{
    public class SimpleItem
    {
        public string Name { get; set; }
        public SimpleItem (string name)
        {
            Name = name;
        }
    }
    class MyViewModel : ViewModelBase
    {
        private ObservableCollection<SimpleItem> dataItems = new ObservableCollection<SimpleItem>();
        public ObservableCollection<SimpleItem> DataItems
        {
            get { return dataItems; }
            set { dataItems = value; }
        }

        private SimpleItem selItem;

        public SimpleItem SelItem
        {
            get { return selItem; }
            set {
                selItem = value;
                OnPropertyChanged(() => SelItem);
            }
        }


        public MyViewModel()
        {
            isDone = true;
            dataItems.Add(new SimpleItem("one"));
            //dataItems.Add(new SimpleItem("two"));
            //dataItems.Add(new SimpleItem("three"));
            //dataItems.Add(new SimpleItem("one"));
            //dataItems.Add(new SimpleItem("two"));
            //dataItems.Add(new SimpleItem("three"));
            //dataItems.Add(new SimpleItem("one"));
            //dataItems.Add(new SimpleItem("two"));
            //dataItems.Add(new SimpleItem("three"));
        }

        private DelegateCommand testCommand;
        public ICommand TestCommand
        {
            get
            {
                if (testCommand == null)
                {
                    testCommand = new DelegateCommand((parameter) => TestLogic(), (parameter) => CanTest());
                }
                return testCommand;
            }
        }

        private bool CanTest()
        {
            return true;
        }

        private bool isDone;

        private async void TestLogic()
        {
            if (isDone && dataItems.Count <= 30)
            {
                isDone = false;

                DataItems.Add(new SimpleItem("Loading"));
                var loadedItems = await AddLogicAsync();
                DataItems.RemoveAt(DataItems.Count - 1);
                foreach (SimpleItem loadedItem in loadedItems)
                {
                    DataItems.Add(loadedItem);
                }
                isDone = true;
            }

        }


        private DelegateCommand addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new DelegateCommand((parameter) => AddLogic(), (parameter) => CanAdd());
                }
                return addCommand;
            }
        }

        private bool CanAdd()
        {
            return true;
        }

        private void AddLogic()
        {
            if (isDone == false && DataItems.Count >= 1)
            {
                DataItems.RemoveAt(DataItems.Count - 1);
                isDone = true;
            }
            DataItems.Add(new SimpleItem("one"));
            DataItems.Add(new SimpleItem("two"));
            DataItems.Add(new SimpleItem("three"));
        }

        private async Task<IEnumerable<SimpleItem>> AddLogicAsync()
        {
            await Task.Delay(5000);
            return new SimpleItem[3] { new SimpleItem("loaded-one"), new SimpleItem("loaded-two"), new SimpleItem("loaded-three") }.AsEnumerable();
        }
    }
}
